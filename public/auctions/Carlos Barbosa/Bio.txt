<p>
    Carlos Barbosa
    <br/>
    <b>2004 – 2021</b>
    <ul>
        <li>President of the Automobile Club of Portugal</li>
        <li>General Chairman of the Board of the Portuguese Road Safey Autority (PRP - Prevenção Rodoviária Portuguesa)</li>
        <li>Member of the board of Portuguese National Sporting Authority for Motorsports and Karting (FPAK - Federação Portuguesa de Automobilismo e Karting)</li>
        <li>Deputy of the City Council of Lisbon</li>
        <li>Member to the Management Council</li>
        <li>Member to the  Eurocouncil</li>
        <li>Member to the Eurobord FIA Region I</li>
        <li>Region I Representative of the FIA in World Mobily Council for Automobile and Tourism, proposed for election at the FIA General Assembly in October 2009</li>
        <li>Representative of the FIA (International Automobile Federation) to the European Commission for all matters connected with Motor Sport</li>
        <li>President  of Founding Member´s Club</li>
    </ul>
</p>
