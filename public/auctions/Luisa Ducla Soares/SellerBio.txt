<p>
    Luísa Ducla Soares nasceu em Lisboa  em 1939. 
    Licenciada em Filologia Germânica, iniciou a sua carreira com um livro de poesia, Contrato ,  mas dedica-se especialmente à literatura infanto-juvenil como autora, estudiosa, divulgadora.  Foi tradutora, consultora literária, jornalista, adjunta do Ministério da Educação e trabalhou 30 anos na Biblioteca Nacional. 
    Recusou, por motivos políticos, o Prémio Maria Amália Vaz de Carvalho , que pretenderam , em 1973, atribuir ao seu primeiro livro de literatura infantil, A História da Papoila , por se negar a receber galardões de um governo alicerçado na censura.
    Foi duas vezes galardoada pela Fundação Calouste Gulbenkian.  Em  1986 , com o prémio pelo Melhor Texto de Literatura para Crianças do Biénio 1984-1985 atribuído a 6 histórias de encantar e dez anos mais tarde foi-lhe entregue o prémio desta mesma instituição  pelo conjunto da obra.
    Foi nomeada como representante de Portugal para o Prémio Hans Christian Andersen do IBBY,  para o Prémio Iberoamericano SM  de Literatura Infantil e Juvenil e para o Prémio Alma.
    Luísa Ducla Soares é autora de mais de 180 obras , tendo também colaborado regularmente em diversos periódicos com contos infantis, poesia, artigos e crónicas. 
    A pedido de diversas editoras tem-se empenhado na elaboração de manuais escolares.
    A UNICEF e a OIKOS organizaram uma maleta pedagógica e uma notável exposição itinerante , tendo em vista o repúdio de racismo e a  compreensão entre os povos, baseadas no seu conto Meninos de todas as cores .
    Escreveu o guião intitulado Alhos e Bugalhos, série televisiva sobre a língua portuguesa, transmitida pela RTP durante as Comemorações do Ano Europeu das Línguas (2001). 
    Elaborou o site da página da Presidência da República para os mais novos no mandato de Jorge Sampaio. 
    No campo musical foram editados vários livros-CD com poemas de sua autoria musicados por Suzana Ralha, João Portugal, Daniel Completo , entre outros.  
    É sócia fundadora do Instituto de Apoio à Criança.
    Participa regularmente em sessões e projetos de divulgação e animação cultural em escolas , bibliotecas, congressos, universidades. 
    Se nos seus textos aparece um universo de valores caros ao desenvolvimento intelectual, moral, cívico e social das crianças, nunca renega o papel da imaginação, da fantasia e do humor.
</p>