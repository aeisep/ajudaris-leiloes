<p>
Nasceu no Porto, é Mãe e divide a casa com a família, os livros e os gatos. Dizem que nasceu já a tagarelar e adora contar histórias. Esteve como jornalista na RTP, nomeadamente no programa Sociedade Civil; em 2013 criou a sua própria empresa de comunicação, media training e conteúdos.
Co-fundadora da Associação Nuvem Vitória para, todas as noites e com a ajuda de voluntários, levar contos de embalar a crianças internadas.
Foi Presidente do Ano Europeu do Voluntariado em 2011 e recebeu, em 2013, a Ordem de Mérito Civil.

</p>