<p>
Cruzei-me com este Santo António, num Domingo em família, quando visitava uma loja de cerâmica. Fiquei logo encantada por esta peça amor. Há algo de sereno e bondoso na expressão dele. Tem ocupado lugar de destaque na minha sala. Agora está na hora de ir espalhar bondade e paz para a casa de outra pessoa que tenha a bondade de aceitá-lo e acolhê-lo.
</p>