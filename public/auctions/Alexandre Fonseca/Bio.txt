<p>
Telefone de mesa, da marca ATM - Automatic Telephone Manufacturers,
modelo Aptofone. O telefone é constituído no seu todo por baquelite preto e metal no
marcador. O corpo do telefone tem forma cónica achatada, possuindo na parte superior
uma estrutura em forma de "T", a qual possui um encaixe próprio para a colocação do
microtelefone na posição de descanso. Possui, ainda nesta estrutura, uma patilha ligada a
uma mola interruptora que serve para ligar ou desligar o sinal de recepção da linha de rede
sempre que, respectivamente, se levanta ou se pousa o microtelefone.
</br>
A base do telefone tem um formato redondo e encaixado nessa base encontra-se o marcador de disco rotativo
em metal preto. O marcador de disco rotativo tem dez alvéolos, sob o qual existe uma placa
numerada de "1" a "0". Na coroa central do marcador existe um espaço em branco para a
colocação do número de telefone ou extensão correspondente. O microtelefone é em
baquelite de cor preta e reúne na mesma peça o auscultador e microfone. 
</br>
Na parte posterior do corpo central possui, um alvéolo para a saída de dois cabos telefónicos de cor
preta, revestidos a plástico, sendo um para ligação ao microtelefone e outro terminado com
uma ficha própria para ligação à rede telefónica pública.
</p>