<p>
Estes foram os meus primeiros sapatos de ballet. 
</br>
Trazem-me muito boas memórias. 
</br>
A dança surgiu numa fase complicada da minha vida e na altura teve um papel fundamental na minha reorganização a nível pessoal. 
Costumo dizer que a dança me salvou. Hoje, continua a ser uma das coisas mais importantes da minha vida, e a que me deixa mais realizada. 
Por esse motivo, estes sapatos têm um simbolismo enorme e representam, sem duvida, o início da minha história com a dança.  
</p>