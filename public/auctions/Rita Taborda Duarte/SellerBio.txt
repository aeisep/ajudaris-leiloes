<p>
    Rita Taborda Duarte nasceu em Lisboa, em 1973. É poeta, professora do ensino superior e escritora de livros para a infância. Em 1998, publica o seu primeiro livro de poesia (Poética Breve, Black Sun Editores), a que se seguiram outros dois:  Na estranha Casa de um Outro (2006)  e Dos Sentidos das Coisas (2007) , escritos ao abrigo de uma bolsa de criação literária atribuída pelo Instituto Português do Livro e das bibliotecas, em 2005.  Em 2003, vence o prémio Branquinho da Fonseca Expresso-Gulbenkian, com o livro A Verdadeira História da Alice. A partir daí, tem escrito com regularidade para crianças e jovens, contando com uma dezena de obras publicadas, muitas delas incluídas no Plano Nacional de Leitura. Em 2015 publica o livro de poesia  Roturas e Ligamentos (Abysmo) em parceria com André da Loba (ilustrações) e em 2019 o livro As Orelhas de Karenin  ( Abysmo, com ilustrações de Pedro Proença),  finalista dos prémios Correntes de Escrita e  SPAutores 2020. Tem sido autora convidada em diversos festivais de poesia internacionais.

    Poesia

    ●	Pequeno Livro das Pedras, Lisboa, Nova Mymosa, 2021 ( plaquette)
    ●	As Orelhas de Karenin, Lisboa, Abysmo, 2019, (com ilustrações de Pedro Proença) 
    ●	Roturas e Ligamentos. Lisboa, Abysmo, 2015 ( com ilustrações de André da Loba)
    ●	Elogio do Outono, Lisboa, o homem do saco, 2014 ( concepção gráfica Luís Henriques)
    ●	Experiências Descritivas: Dos sentidos das coisas/Círculos, Lisboa, Editorial Caminho, 2007 (Co-autoria de André Barata, com ilustrações de Luís Henriques)
    ●	Na Estranha Casa de Um Outro: Esboço de uma biografia poética, Lisboa, Asa, 2006
    ●	Poética Breve, Lisboa, Black Sun Editores, 1998

    Literatura para Crianças
    
    ●	O Leão sem Juba, o Elefante sem Tromba e a Casa sem Telhado, Caminho, 2020 (ilustações de Raquel Caiano)
    ●	Um Hotel de Muitas Estrelas,FEC, 2019
    ●	Animais e Animenos, Lisboa, Caminho, 2017 ( ilustrações de Pedro Proença)  
    ●	O Rapaz que não se tinha Quieto, Lisboa, Caminho, 2014 (ilustrações de Ana Ventura)
    ●	O Manel e o Miúfa, o Medo Medricas, Lisboa, Caminho, 2012 (ilustrações de Maria João  Lima)
    ●	Trapalhadas Azaradas com Bolo de Chantilly, APCC, 2011 (Ilustrações Maria João Worm)
    ●	Teófilo Braga: Para além do Horizonte Azul, Direcção Regional dos Açores, 2011 (ilustrações Luís Henriques)
    ●	Gastão, Vida de Cão, Editorial Caminho, 2010 (ilustrações Luís Henriques)
    ●	Fred e Maria, Editorial Caminho, 2009 (Ilustrações de Luís Henriques) (seleccionado pelo Plano o Nacional de Leitura)
    ●	Sabes, Maria, o Pai Natal não Existe, Editorial Caminho, 2008, Ilustrações de Luís Henriques (seleccionado para Plano Nacional de Leitura)
    ●	O Tempo Canário e o Mário ao Contrário, Editorial Caminho, 2008, ilustrações de Luís Henriques
    ●	Os Piolhos do Miúdo e os Miúdos do Piolho, Editorial Caminho, 2007, ilustrações de Luís Henriques
    ●	A Família dos Macacos, Editorial Caminho, 2006, ilustrações de Luís Henriques (nomeado pela White Ravens - Feira Internacional Infantil de Bolonha)
    ●	“O Rapaz que não se tinha quieto” in Quatro Histórias com Barão, Homenagem a Branquinho da Fonseca, Lisboa, FCG, 2005 (Vol. colectivo).
    ●	A Verdadeira História de Alice, Lisboa, Editorial Caminho, 2004 (premiado em 2003, com o prémio Branquinho da Fonseca Expresso/Gulbenkian; seleccionado para o Plano Nacional de Leitura)
</p>