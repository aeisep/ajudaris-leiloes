<p>
Carla Anjos é ilustradora e Professora de desenho e pintura. Vive em Penafiel onde tem o seu atelier e onde desenvolve projetos ligados à ilustração. Apaixonada pela ilustração e livros infantis, trabalha cada livro como um desafio e um sonho. Gosta de lugares calmos e de harmonia e tenta importar isso para os seus trabalhos.
</p>