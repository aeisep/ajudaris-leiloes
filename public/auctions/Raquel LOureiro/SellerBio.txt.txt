<p>
Artista, Empreendedora, fundadora da empresa RL Lifestyle, Premium Concierge e Private Broker da Sotheby´s International Realty Portugal. 
A reconhecida super modelo, é apresentadora, atriz, dj,  embaixadora de beleza na América do Sul e já foi rosto de marcas como a Bentley, GQ entre outras.
Com formação na área de moda, representação, apresentação, música, gestão, organização e produção de eventos, marketing, relações públicas e imobiliária de luxo, Raquel Loureiro é um nome de grande referência em Portugal. 
Nasceu e cresceu com a música e foi vocalista de uma banda da editora Sony Music Portugal, a Dj formada na Prodj, tem no seu currículo atuações nos mais exclusivos hotéis de luxo, eventos privados como a receção á família real do Mónaco em Portugal ou a marca de joalharia Bulgari, é também autora do livro “ A Música que toca a Alma”, uma homenagem ao seu Pai, o Maestro José Duarte Loureiro. O seu estilo musical é totalmente eclético, desde o deep house, tech house, melodic & techno até ao soulful house, organic house, pop, indie, fazem das suas atuações  momentos únicos e exclusivos.

</p>