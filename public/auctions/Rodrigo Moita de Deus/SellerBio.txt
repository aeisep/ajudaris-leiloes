<p>
    43 anos. Três filhos. Dois livros. Várias árvores.
    Diretor do NewsMuseum, CEO da Associação Portuguesa de Centros Comerciais entre outras coisas. 
    Consultor, comentador da RTP, conferencista, um dos criadores do blog 31 da Armada e às vezes pai de três filhos. 
    Do ponto de vista ideológico define-se muito simplesmente como agitador, conservador, social-Marialva, neo-liberal, ativista, monárquico não praticante. 
</p>