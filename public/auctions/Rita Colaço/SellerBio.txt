<p>
    Rita Colaço nasceu em Mação em 1979. Cresceu no meio de rádios-piratas, mas licenciou-se
    em Geografia e Planeamento Regional, na Universidade Nova, em Lisboa. Em 2001, fez o
    Curso de Especialização em Jornalismo, no CENJOR. Na Antena 1 desde 2003, atualmente é
    coordenadora do programa Grande Reportagem, dedicando-se, sobretudo, a contar histórias sobre vidas que vivem nas franjas da sociedade. Já recebeu duas vezes o Prémio Gazeta de
    Rádio, esteve nomeada para o Prix Europa, com a grande reportagem “Jamaika também é
    Portugal” e também nomeada para o Prémio Gabo com a grande reportagem "Com olhos de ouvir". Autora do podcast "O som da minha vida" e mãe de dois rapazes cheios de vida. É artesã-amadora nas horas (muito pouco) vagas.
</p>