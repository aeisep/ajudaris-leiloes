<p>
Nascido em Tigre, Buenos Aires na Argentina, Chakall cedo descobriu a
culinária. Cresceu na cozinha do restaurante da mãe e é já a quarta
geração da família dedicada à arte da culinária. 
</br>
“Filho” de 5 culturas (tanto
quanto se sabe!) com pais de origem galega, suíça-alemã, basco-francês,
italiana e indígena do norte da Argentina, Chakall tem em duas mulheres a
sua maior fonte de inspiração: a mãe Susana e a madrinha Virgínia. Foram
elas que lhe ensinaram a importância de comer e o respeito por aquilo
que se come.
</p>
<p>
É destas raízes, mistura de culturas, sabores e saberes e
também da enorme curiosidade e das viagens que tem feito ao longo da
vida que cresce o seu gosto pela cozinha e, mais que isso, a experiência de
vida através da comida. “Obrigado” a servir às mesas, levantar mesas,
arranjar salsa e outras tarefas, aos 14 já tomava conta da cozinha do
restaurante da progenitora. 
</br>
Aos 18 anos e farto do cheiro a fritos e de
afugentar as candidatas a namoradas, Chakall deixa a cozinha e estuda
Jornalismo, tendo sido jornalista e crítico de música no jornal argentino El
Cronista. Mas passados 7 anos percebeu que o jornalismo não era,
também, o que queria da vida. Deixou tudo para trás e abraçou outra das
suas paixões: viajar!
</p>