<p>
Joana Teles é apresentadora de Televisão na RTP desde 2007, contando com um vasto currículo na estação pública com dezenas de programas apresentados. É considerada como dos rostos mais queridos da estação pública.
Foi modelo fotográfico de várias campanhas publicitárias, desfiles e catálogos para marcas, como Maybelline New-York, Custo Barcelona, Scripta, etc...
Em 2015 cria a sua marca de roupa materno-infantil, a <b>BBme by Joana Teles</b>.
</br>
Atualmente apresenta o programa Aqui Portugal, na RTP1 e RTP Internacional.
</p>